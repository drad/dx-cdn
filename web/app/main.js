/*
 * the keymap data logic.
 *   note: version is stored in version.json as it is served via lighttpd.
 */
var year = new Date().getFullYear()
function load() {
    //console.log("onload...");
    // set version
    //document.getElementById("app-version").innerHTML = "v." + app_version;
    document.getElementById("copyright").innerHTML = "&copy; " + year + " <a href='https://dradux.com'>dradux.com</a>. All rights reserved.";
}
