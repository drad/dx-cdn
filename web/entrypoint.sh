#!/bin/sh

chmod a+w /dev/stderr
chmod a+w /dev/stdout

echo "- starting rsyslogd daemon..."
rsyslogd

echo "starting lighttpd..."
exec lighttpd -D -f /etc/lighttpd/lighttpd.conf
