# https://hub.docker.com/_/alpine
FROM alpine:3.21
MAINTAINER drad <sa@adercon.com>

ARG DEPLOY_ENV

COPY entrypoint.sh /
COPY config/lighttpd/*.* /etc/lighttpd/
COPY app/* /var/www/localhost/htdocs/

WORKDIR /var/www/localhost/htdocs

RUN apk upgrade                                                                \
    && apk add --no-cache                                                      \
        tzdata                                                                 \
        lighttpd                                                               \
        rsyslog                                                                \
  # set timezone                                                               \
  && cp "/usr/share/zoneinfo/America/New_York" /etc/localtime                  \
  # fix rsyslogd imklog permission issue by removing kernel log                \
  && sed -i '/imklog/s/^/#/' /etc/rsyslog.conf                                 \
  # create lighttpd cache dir                                                  \
  && mkdir /var/cache/lighttpd                                                 \
  && chown -R lighttpd:lighttpd /var/cache/lighttpd                            \
  # cleanup                                                                    \
  && apk del tzdata                                                            \
  && chmod +x /entrypoint.sh                                                   \
  && rm -f /var/cache/apk/*                                                    \
  && echo "Setup complete!"

# the js data should be mounted on the following volume.
VOLUME /var/www/localhost/htdocs/data

EXPOSE 80
CMD /entrypoint.sh
