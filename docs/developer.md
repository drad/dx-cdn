# Developer


### How To Add Media to CDN

- CDNs data dir is located on rancher at: `/tank2/dxcdn`
- make a directory for your content such as: `runrader` or `runrader/team`
- rsync data to directory as needed
- data will be available at: `https://cdn.dradux.com/data/runrader/team`
