# README

The DX CDN - focused on providing fast media serving to other apps.

### Architecture

- ✓ lighttpd
- ✗ varnish

The application has been dockerized, you can run it via docker-compose.

The application has been designed to run in k8s with the app 'data' stored in a PV/C, see app/k8s/* for details.


### Deployment

#### k8s/helm

This application is fully deployable via helm, go to the `.devops/helm` directory and perform the following to deploy:

- configure the `values.yaml.prod` file per your needs
- ensure the `dx-cdn` nfs share has been set up (pv and pvc will be created but the share itself needs to be created)
- deploy the chart: `helm install dx-cdn dx-cdn/ --values dx-cdn/values.yaml.prod --create-namespace --namespace dx-cdn --disable-openapi-validation`
  - update: `helm upgrade dx-cdn dx-cdn/ --values dx-cdn/values.yaml.prod --namespace dx-cdn --disable-openapi-validation`
  - uninstall: `helm uninstall -n dx-cdn dx-cdn`
  - NOTICE: after deploying the updated chart you need to rollout the change to the service impacted:
    + `kubectl rollout restart -n dx-cdn deployment dx-cdn-web varnish`
